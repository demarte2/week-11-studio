# Week 11 studio

For this studio, you should write a program that asks the user for a Twitter handle. It should then use the Twitter API to retrieve the most recent tweets from that person, and draw a word cloud on the screen that illustrates what those tweets say.